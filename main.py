import serial
import platform
import regex_spm

debug = False

print(platform.system())

match platform.system():
    case "Windows":
        ComPort = "COM3"
    case "Linux":
        ComPort = "/dev/ttyUSB1"
    case _:
        print("Unknown OS: "+ platform.system())
        exit(1)

BaudRate = 115200
printTime = ""

def initSerial():
    print("Initializing Serial")
    Ser = serial.Serial(ComPort, BaudRate)
    return Ser

def printDone():
    print("PRINT DONE")
    print("Print Time: "+printTime)
    #TODO: Send Notifications!

def parseLine(line):
    line = line.strip()
    #print(ord(line[-1]))
    match regex_spm.fullmatch_in(line):
        case r"T:(\d+.\d+) /(\d+.\d+) B:(\d+.\d+) /(\d+.\d+) @:(\d+) B@:(\d+) W:([\d?]+)" as m:
            print("Extruder Temp: "+ m[1]+"/"+m[2],end="")
            print(" Bed Temp: "+ m[3]+"/"+m[4])
        case r"echo:Print time: ([\S\s]+)" as m:
            global printTime
            printTime = m[1]
        case r"echo:([\S\s]+)" as m:
            print(m[1])
        case r"X:([\d-]+.\d+) Y:([\d-]+.\d+) Z:([\d-]+.\d+) E:([\d-]+.\d+) Count X:([\d-]+) Y:([\d-]+) Z:([\d-]+)" as m:
            X=m[1]
            Y=m[2]
            Z=m[3]
            #print("Extruder Temp: "+ m[1]+"/"+m[2],end="")
        case r"File opened: ([\d\D]+.gco) Size: (\D+)" as m:
            currFile = m[1]
            print("PRINTING "+currFile)
        case "show_bootscreen":
            print("Displaying Boot Image")
        case "load config sucess":
            print("Successfully Loaded config")
        case "Done printing file":
            printDone()
        case "OK":
            print("TODO: Something something sending stuff")
        case _:
            print(line)

def main():
    print("Hello World!")
    if not debug:
        printer = initSerial()
    else:
        #printer = open('3D-Printer_Auto_Leveling.log', 'r')
        #printer = open('3D-Printer_Cat.log', 'r')
        printer = open('3D-Printer_Boot-to-Fox.log', 'r')
    #printer.open()
    while True:
        line = printer.readline().decode("utf-8")
        if len(line)==0:
            break
        #print(line)
        parseLine(line)


if __name__ == "__main__":
    main()